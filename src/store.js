import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import createPersistedState from 'vuex-persistedstate';

Vue.use(Vuex);

export default new Vuex.Store({
  plugins: [
    createPersistedState(),
  ],
  state: {
    auth: {
      token: null,
      loggedIn: false,
      expiresIn: null,
    },
  },
  mutations: {
    AUTHENTICATE(state, {
      access_token: token,
      expires_in: expiresIn,
    }) {
      Object.assign(state, {
        auth: {
          token,
          loggedIn: true,
          expiresIn,
        },
      });
    },
    LOGOUT(state) {
      Object.assign(state, {
        auth: {
          token: null,
          loggedIn: false,
          expiresIn: null,
        },
      });
    },
  },
  actions: {
    async products(context, page = 1) {
      const { data } = await axios.get(`products?page=${page}`);

      return data;
    },
    async login({
      commit,
    }, {
      email,
      password,
    }) {
      const { data } = await axios.post('auth/customer', {
        email,
        password,
      });

      commit('AUTHENTICATE', data);
    },
    logout({
      commit,
    }) {
      commit('LOGOUT');
    },
    async like(context, id) {
      const { data } = await axios.post(`products/${id}/likes`);

      return data;
    },
    async buy(context, id) {
      const { data } = await axios.post(`products/${id}/buy`);

      return data;
    },
  },
  getters: {
    token(state) {
      return state.auth.token;
    },
  },
});
