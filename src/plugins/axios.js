import axios from 'axios';
import store from '../store';

axios.interceptors.request.use((request) => {
  const { token } = store.getters;

  if (token) {
    request.headers.common.Authorization = `Bearer ${token}`;
  }

  request.baseURL = process.env.VUE_APP_API_URL;

  return request;
});
